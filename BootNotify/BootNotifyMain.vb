﻿'This file is part of BootNotify.exe.

'BootNotify.exe is free software: you can redistribute it and/or modify
'it under the terms of the GNU Lesser Public License as published by
'the Free Software Foundation, either version 3 of the License, or
'(at your option) any later version.

'BootNotify.exe is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
'GNU Lesser Public License for more details.

'You should have received a copy of the GNU Lesser Public License
'along with BootNotify.exe.  If not, see <http://www.gnu.org/licenses/>.

Imports System.Net
Imports System.IO
Imports System.Text

Module BootNotifyMain

    Sub Main(ByVal args() As String)
        If args.Length = 0 Then
            Console.WriteLine("Missing User Token")
            Exit Sub
        ElseIf args.Length = 1 Then
            Dim userToken As String = args(0)
            Send(userToken, My.Computer.Name.ToString() & " now online.")
        Else
            Console.WriteLine("Too many arguments")
            Exit Sub
        End If
    End Sub

    Sub Send(ByVal userToken As String, ByVal msg As String)
        Dim request As HttpWebRequest
        Dim response As HttpWebResponse = Nothing
        Dim reader As StreamReader
        Dim address As Uri
        Dim appToken As String
        Dim data As StringBuilder
        Dim byteData() As Byte
        Dim postStream As Stream = Nothing

        address = New Uri("https://api.pushover.net/1/messages.json")

        ' Create the web request
        request = DirectCast(WebRequest.Create(address), HttpWebRequest)

        ' Set type to POST
        request.Method = "POST"
        request.ContentType = "application/x-www-form-urlencoded"

        ' Create the data we want to send
        appToken = "aTxcAwRt75NnKi488Uz6ahz4zfbeuN"

        data = New StringBuilder()
        data.Append("token=" + WebUtility.HtmlEncode(appToken))
        data.Append("&user=" + WebUtility.HtmlEncode(userToken))
        data.Append("&message=" + WebUtility.HtmlEncode(msg))

        ' Create a byte array of the data we want to send
        byteData = UTF8Encoding.UTF8.GetBytes(data.ToString())

        ' Set the content length in the request headers
        request.ContentLength = byteData.Length

        ' Write data
        Try
            postStream = request.GetRequestStream()
            postStream.Write(byteData, 0, byteData.Length)
        Finally
            If Not postStream Is Nothing Then postStream.Close()
        End Try

        Try
            ' Get response
            response = DirectCast(request.GetResponse(), HttpWebResponse)

            ' Get the response stream into a reader
            reader = New StreamReader(response.GetResponseStream())

            ' output
        Finally
            If Not response Is Nothing Then response.Close()
        End Try

    End Sub 'sub main()

End Module
