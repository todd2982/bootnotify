﻿##Readme  

Used to send [pushover](https://pushover.net/) notification to device as push notification on computer startup. Intended to be run as a startup script or other similar method.  

#Syntax:
    BootNotify.exe UserToken

#License:
Released under LGPLv3